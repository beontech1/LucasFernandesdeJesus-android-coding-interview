package com.beon.androidchallenge.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.beon.androidchallenge.data.repository.FactRepository
import com.beon.androidchallenge.domain.model.Fact
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

sealed class FactResult {
    data class Success(val fact: Fact): FactResult()
    data class Error(val message: String): FactResult()
    object Empty: FactResult()
    object Loading: FactResult()
}

class MainViewModel : ViewModel() {

    val currentFact = MutableLiveData<FactResult>()

    private val typingDelay: Long = 1000L
    private var searchJob: Job? = null

    fun searchNumberFact(number: String) {

        currentFact.value = FactResult.Loading

        if (number.isEmpty()) {
            currentFact.postValue(FactResult.Empty)
            return
        }

        //cancelling the job in case the user requests again
        searchJob?.cancel()

        searchJob = viewModelScope.launch {
            delay(typingDelay)

            FactRepository.getInstance().getFactForNumber(number, object : FactRepository.FactRepositoryCallback<Fact> {
                override fun onResponse(response: Fact) {
                    currentFact.postValue(FactResult.Success(response))
                }

                override fun onError() {
                    currentFact.postValue(FactResult.Error("An error ocurred"))
                }
            })
        }
    }
}