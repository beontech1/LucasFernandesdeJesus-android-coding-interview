package com.beon.androidchallenge.data.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "facts")
data class FactEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0,
    var text: String? = null,
    var number: Long? = null,
    var found: Boolean? = null,
    var type: String? = null
)