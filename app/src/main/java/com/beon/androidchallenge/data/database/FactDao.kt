package com.beon.androidchallenge.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface FactDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFact(fact: FactEntity)

    @Query("SELECT * FROM facts")
    fun getFacts(): List<FactEntity?>
}