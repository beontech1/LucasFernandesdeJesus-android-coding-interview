package com.beon.androidchallenge.data.repository

import com.beon.androidchallenge.data.database.FactDao
import com.beon.androidchallenge.data.database.FactDatabase
import com.beon.androidchallenge.data.database.FactEntity

class FactLocalDataSource {

    fun insertFact(fact: FactEntity) {
        FactDatabase().factDao().insertFact(fact)
    }

    fun getFacts(): List<FactEntity?> {
        return FactDatabase().factDao().getFacts()
    }

}