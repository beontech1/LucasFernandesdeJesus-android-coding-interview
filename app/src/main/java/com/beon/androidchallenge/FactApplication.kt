package com.beon.androidchallenge

import android.app.Application
import androidx.room.Room
import com.beon.androidchallenge.data.database.FactDatabase

class FactApplication: Application() {

    override fun onCreate() {
        super.onCreate()

        database = Room.databaseBuilder(
            this,
            FactDatabase::class.java,
            "fact_database"
        ).build()
    }

    companion object {
        lateinit var database: FactDatabase
    }
}